---
title: "GitLab Project Management - Hands-On Lab Overview"
description: "This Hands-On Guide walks you through the lab exercises used in the GitLab Project Management course."
---

# GitLab Project Management

## Lab Guides

> **We are transitioning to the latest version of this course.** If your group URL starts with `https://spt.gitlabtraining.cloud`, please use the v15 instructions.

| Lab Guide | Version 15 | Version 16 |
|-----------|------------|------------|
| Lab 1: Access the GitLab training environment | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab1.md) | [v16 Lab Guide](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab1/) |
| Lab 2: Create an organizational structure in GitLab | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab2.md) | [v16 Lab Guide](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab2/) |
| Lab 3: Use GitLab planning tools | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab3.md) | [v16 Lab Guide](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab3/) |
| Lab 4: Create issues | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab4.md) | [v16 Lab Guide](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab4/) |
| Lab 5: Organize and manage issues | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab5.md) | [v16 Lab Guide](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab5/) |
| Lab 6: Use a merge request to review and merge code | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab6.md) | [v16 Lab Guide](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab6/) |
| Lab 7: Create and customize issue boards | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab7.md) | [v16 Lab Guide](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab7/) |
| Lab 8: Create and manage a Kanban board| [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab8.md) | [v16 Lab Guide](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab8/) |
| Lab 9: Create and manage a Scrum board | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab9.md) | [v16 Lab Guide](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab9/) |
| Lab 10: Create and manage a Waterfall board | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab10.md) | [v16 Lab Guide](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab10/) |

## Quick links

Here are some quick links that may be useful when reviewing this Hands On Guide.

* [GitLab Project Management course description](https://about.gitlab.com/services/education/pm/)
* [GitLab Project Management Specialist certification details](https://about.gitlab.com/services/education/gitlab-project-management-associate/)

## Suggestions?

If you'd like to suggest changes to the *GitLab Project Management Hands-on Guide*, please submit them via merge request.
